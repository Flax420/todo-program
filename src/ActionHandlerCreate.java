import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class ActionHandlerCreate implements ActionListener {
    private MainGUI main;
    private CreateNewItem createNewItem;
    // Get main instance and createnewitem instance
    ActionHandlerCreate (MainGUI main, CreateNewItem createNewItem) {
        this.main = main;
        this.createNewItem = createNewItem;
    }

    public void actionPerformed(ActionEvent event) {
        // if create button is pressed on the creation menu get the text from the input boxes and create a new todoitem
        if (event.getActionCommand().equals("Create")) {
            String text = createNewItem.text.getText();
            int priority = createNewItem.priorityDropDown.getSelectedIndex();
            main.databaseController.insertData(main, text, Integer.toString(priority));
            createNewItem.dispatchEvent(new WindowEvent(createNewItem, WindowEvent.WINDOW_CLOSING));
        }
    }
}