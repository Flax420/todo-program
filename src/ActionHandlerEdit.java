import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class ActionHandlerEdit implements ActionListener {
    private MainGUI main;
    private EditItem editItem;
    private int editIndex;
    private int id;
    // Get main instance and createnewitem instance
    ActionHandlerEdit (MainGUI main, EditItem editItem, int editIndex, int id) {
        this.main = main;
        this.editItem = editItem;
        this.editIndex = editIndex;
        this.id = id;
    }

    public void actionPerformed(ActionEvent event) {
        // if create button is pressed on the creation menu get the text from the input boxes and create a new todoitem
        if (event.getActionCommand().equals("Apply")) {
            String text = editItem.text.getText();
            String priority = Integer.toString(editItem.priorityDropDown.getSelectedIndex());
            String itemID = Integer.toString(id);

            main.databaseController.updateData(main, text, priority, itemID);

            editItem.dispatchEvent(new WindowEvent(editItem, WindowEvent.WINDOW_CLOSING));
        }
    }
}