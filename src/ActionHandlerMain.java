import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionHandlerMain implements ActionListener {
    private MainGUI main;

    // Get main instance
    ActionHandlerMain(MainGUI main) {
        this.main = main;
    }

    // If new is pressed make new instance for createnewitem gui
    public void actionPerformed(ActionEvent event) {
        if (event.getActionCommand().equals("New")) {
            CreateNewItem newItemGui = new CreateNewItem(main);
        } else if (event.getActionCommand().equals("Remove")) {
            RemoveItem remove = new RemoveItem(main);
        } else if (event.getActionCommand().equals("Edit")) {
            if(main.list.getSelectedIndex() != -1) {
                EditItem editItem = new EditItem(main, main.list.getSelectedIndex());
            }
        }
    }
}