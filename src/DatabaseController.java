import java.sql.*;

public class DatabaseController {

    // Most of this code was taken from here and tweaked https://www.tutorialspoint.com/sqlite/sqlite_java.htm
    public void insertData(MainGUI main, String title, String priority) {
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:todo.db");
            c.setAutoCommit(false);
            // Opened database correctly

            stmt = c.createStatement();
            String sql = "INSERT INTO TODO (Title, Priority) " +
                    "VALUES ( '" + title + "', " + priority + " );";
            stmt.executeUpdate(sql);

            stmt.close();
            c.commit();
            c.close();
        } catch ( Exception e ) {
            // Database did not open correctly
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        main.databaseController.getData(main);
    }

    public void getData(MainGUI main){

        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:todo.db");
            c.setAutoCommit(false);
            // Opened database correctly

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery( "SELECT * FROM TODO;" );

            main.items.clear();

            while ( rs.next() ) {
                int id = rs.getInt("ID");
                String  title = rs.getString("Title");
                int priority  = rs.getInt("Priority");

                main.items.add(new TodoItem(id, title, priority));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            // Database did not open correctly
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            main.databaseController.createTable(main);
        }
        // Refresh list
        main.renderList(main);
    }

    public void updateData(MainGUI main, String title, String priority, String id){
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:todo.db");
            c.setAutoCommit(false);
            // Opened database correctly

            stmt = c.createStatement();
            String sql = "UPDATE TODO set Title = '" + title + "', Priority = " + priority + " where ID=" + id + ";";
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            // Database did not open correctly
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        // Refresh list
        main.databaseController.getData(main);
    }

    public void deleteData(MainGUI main, String id){
        Connection c = null;
        Statement stmt = null;

        try {
            // Opened database correctly
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:todo.db");
            c.setAutoCommit(false);


            stmt = c.createStatement();
            String sql = "DELETE from TODO where ID=" + id + ";";
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            // Database did not open correctly
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        // Refresh list
        main.databaseController.getData(main);
    }

    public void createTable(MainGUI main){
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:todo.db");

            stmt = c.createStatement();
            String sql = "CREATE TABLE TODO " +
                    "(ID INTEGER PRIMARY KEY     NOT NULL," +
                    " Title           TEXT    NOT NULL, " +
                    " Priority            INTEGER     NOT NULL);";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        main.databaseController.getData(main);
    }
}

