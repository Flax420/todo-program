import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EditItem extends JFrame {
    private MainGUI main;
    private GridBagConstraints gridBagConstraints = new GridBagConstraints();

    // Create public variables for input boxes, unsure of why these did not otherwise work
    public JTextField text = new JTextField(25);

    private String[] priorityStrings = {"None", "Low", "Medium", "High"};

    public JComboBox priorityDropDown = new JComboBox(priorityStrings);

    // Create GUI
    EditItem(MainGUI main, int editIndex) {
        // Get main instance
        this.main = main;

        setTitle("Edit Item");
        setVisible(true);
        setAlwaysOnTop(true);
        // Centers frame
        setLocationRelativeTo(null);
        // Setting the frame size
        setSize(new Dimension(400, 200));


        JPanel panel = new JPanel(new BorderLayout());
        JPanel eastPanel = new JPanel(new GridBagLayout());
        JPanel westPanel = new JPanel(new GridBagLayout());

        JLabel title = new JLabel("Create New TODO Item");

        JLabel enter_title = new JLabel("Enter Title");
        text = new JTextField(main.items.get(editIndex).text, 25);
        priorityDropDown.setSelectedIndex(main.items.get(editIndex).priority);

        JLabel enter_priority = new JLabel("Enter Priority");

        JButton createButton = new JButton("Apply");
        createButton.addActionListener(new ActionHandlerEdit(main, this, editIndex, main.items.get(editIndex).id));

        // Create layout
        panel.add(title, BorderLayout.NORTH);
        panel.add(westPanel, BorderLayout.WEST);
        panel.add(eastPanel, BorderLayout.EAST);
        panel.add(createButton, BorderLayout.SOUTH);

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        westPanel.add(enter_title, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        westPanel.add(enter_priority, gridBagConstraints);

        gridBagConstraints.gridy = 0;
        eastPanel.add(text, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        eastPanel.add(priorityDropDown, gridBagConstraints);
        this.getContentPane().add(panel);
        // Makes it so that the frame kills itself on close
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
}
