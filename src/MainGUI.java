import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class MainGUI extends JFrame {

    public ArrayList<TodoItem> items = new ArrayList<TodoItem>();

    public JList list;
    public DefaultListModel model;
    public JScrollPane scrollPane;

    DatabaseController databaseController;

    public MainGUI() {
        setTitle("TODO");
        setVisible(true);
        // Centers frame
        setLocationRelativeTo(null);
        // Setting the frame size
        setSize(new Dimension(500, 400));
        // Makes it so that the frame exits the program on close
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Creating new elements
        JPanel panelCenter = new JPanel(new BorderLayout());
        JPanel panelNorth = new JPanel(new BorderLayout());
        JPanel panelNorthSubPanel = new JPanel(new GridBagLayout());
        JButton createButton = new JButton("New");
        JButton removeButton = new JButton("Remove");
        JButton editButton = new JButton("Edit");

        model = new DefaultListModel();
        list = new JList(model);
        scrollPane = new JScrollPane(list);
        list.setVisibleRowCount(5);
        scrollPane.setWheelScrollingEnabled(true);

        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        scrollPane.setMinimumSize(new Dimension(100, 50));


        // ActionListeners
        createButton.addActionListener(new ActionHandlerMain(this));
        removeButton.addActionListener(new ActionHandlerMain(this));
        editButton.addActionListener(new ActionHandlerMain(this));

        // DatabaseController
        databaseController = new DatabaseController();
        databaseController.getData(this);


        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        // Adding elements to frame

        scrollPane.setViewportView(list);
        panelCenter.add(scrollPane);
        panelNorth.add(panelNorthSubPanel);
        gridBagConstraints.gridx = 0;
        panelNorthSubPanel.add(createButton, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        panelNorthSubPanel.add(removeButton, gridBagConstraints);
        gridBagConstraints.gridx = 2;
        panelNorthSubPanel.add(editButton, gridBagConstraints);

        this.getContentPane().add(panelNorth, BorderLayout.NORTH);
        this.getContentPane().add(panelCenter, BorderLayout.CENTER);

        this.revalidate();
        this.repaint();
    }

    public static void main(String[] args){
        MainGUI gui = new MainGUI();
    }

    public void renderList(MainGUI main){
        main.model.removeAllElements();
        for(TodoItem item : items){
            String priority;
            switch (item.priority){
                case 0:
                    priority = "None";
                    break;
                case 1:
                    priority = "Low";
                    break;
                case 2:
                    priority = "Medium";
                    break;
                case 3:
                    priority = "High";
                    break;
                    default:
                        priority = "Unknown value";
                        break;
            }
            main.model.addElement(item.text + " - Priority: " + priority);
            main.revalidate();
            main.repaint();
        }
    }
}
