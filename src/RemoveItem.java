
public class RemoveItem {
    private MainGUI main;
    RemoveItem(MainGUI main){
        this.main = main;
        removeItem(main);
    }
    private void removeItem(MainGUI main){
        int itemToRemove = main.list.getSelectedIndex();
        if(itemToRemove != -1){
            main.databaseController.deleteData(main, Integer.toString(main.items.get(itemToRemove).id));
        }
    }
}
