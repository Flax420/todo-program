public class TodoItem {
    // Simple todoitem
    public int id;
    public String text;
    public int priority;
    TodoItem(int id, String text, int priority){
        this.text = text;
        this.priority = priority;
        this.id = id;
    }
}
